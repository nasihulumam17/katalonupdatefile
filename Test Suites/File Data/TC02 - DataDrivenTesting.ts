<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TC02 - DataDrivenTesting</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>f81914af-db2a-4c6f-b224-07e50f9430cf</testSuiteGuid>
   <testCaseLink>
      <guid>1b7c13c3-7cf8-4492-8e35-063e4a9bacac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/S01 - Upload File/TC02 - Data Driven Testing</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>53e41656-bb54-4459-9cb4-7590b97cc421</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/File Data/DataDirektori</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>53e41656-bb54-4459-9cb4-7590b97cc421</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>File</value>
         <variableId>0728a7f3-451a-4273-b020-5451797c067f</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
